# Indexed Colour - a simple Euclidean colour distance setter

Appears under Extensions>Colour>Indexed Colour

Colours can be taken from

▶ Web safe SVG 1.1 palette
▶ .gpl - Gimp palette files
▶ First selected gradient
▶ All gradients contained in document
▶ Colours which appear in bitmap image (via PIL)

▶ Can create square sample swatches for the palette

▶ Euclidean colour distance can be weighted

▶ A bit slow for svgs with large object counts
▶ Does not recolour gradients
▶ Inkscape 1.1+
