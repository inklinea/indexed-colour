#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Indexed Colour a simple Euclidean colour distance setter
# Web safe, or uses external GIMP .gpl palettes https://www.gimp.org
# Inkscape is also able to export to .gpl palettes
# An Inkscape 1.1+ extension
##############################################################################

import inkex
from inkex import Layer, PathElement, LinearGradient

import math
import sys, os, time

from PIL import ImageColor

from inklinea import Inklin

web_safe_gimp_palette = """
GIMP Palette
Name: SVG1.1_Color_Swatch.gpl
#
  0   0   0 #000000
  0   0 128 #000080
  0   0 139 #00008B
  0   0 205 #0000CD
  0   0 255 #0000FF
  0 100   0 #006400
  0 128   0 #008000
  0 128 128 #008080
  0 139 139 #008B8B
  0 191 255 #00BFFF
  0 206 209 #00CED1
  0 250 154 #00FA9A
  0 255   0 #00FF00
  0 255 127 #00FF7F
  0 255 255 #00FFFF
 25  25 112 #191970
 30 144 255 #1E90FF
 32 178 170 #20B2AA
 34 139  34 #228B22
 46 139  87 #2E8B57
 47  79  79 #2F4F4F
 50 205  50 #32CD32
 60 179 113 #3CB371
 64 224 208 #40E0D0
 65 105 225 #4169E1
 70 130 180 #4682B4
 72  61 139 #483D8B
 72 209 204 #48D1CC
 75   0 130 #4B0082
 85 107  47 #556B2F
 95 158 160 #5F9EA0
100 149 237 #6495ED
102  51 153 #663399
102 205 170 #66CDAA
105 105 105 #696969
106  90 205 #6A5ACD
107 142  35 #6B8E23
112 128 144 #708090
119 136 153 #778899
123 104 238 #7B68EE
124 252   0 #7CFC00
127 255   0 #7FFF00
127 255 212 #7FFFD4
128   0   0 #800000
128   0 128 #800080
128 128   0 #808000
128 128 128 #808080
135 206 235 #87CEEB
135 206 250 #87CEFA
138  43 226 #8A2BE2
139   0   0 #8B0000
139   0 139 #8B008B
139  69  19 #8B4513
143 188 143 #8FBC8F
144 238 144 #90EE90
147 112 219 #9370DB
148   0 211 #9400D3
152 251 152 #98FB98
153  50 204 #9932CC
154 205  50 #9ACD32
160  82  45 #A0522D
165  42  42 #A52A2A
169 169 169 #A9A9A9
173 216 230 #ADD8E6
173 255  47 #ADFF2F
175 238 238 #AFEEEE
176 196 222 #B0C4DE
176 224 230 #B0E0E6
178  34  34 #B22222
184 134  11 #B8860B
186  85 211 #BA55D3
188 143 143 #BC8F8F
189 183 107 #BDB76B
192 192 192 #C0C0C0
199  21 133 #C71585
205  92  92 #CD5C5C
205 133  63 #CD853F
210 105  30 #D2691E
210 180 140 #D2B48C
211 211 211 #D3D3D3
216 191 216 #D8BFD8
218 112 214 #DA70D6
218 165  32 #DAA520
219 112 147 #DB7093
220  20  60 #DC143C
220 220 220 #DCDCDC
221 160 221 #DDA0DD
222 184 135 #DEB887
223 223 223 #DFDFDF
224 255 255 #E0FFFF
230 230 250 #E6E6FA
233 150 122 #E9967A
238 130 238 #EE82EE
238 232 170 #EEE8AA
240 128 128 #F08080
240 230 140 #F0E68C
240 248 255 #F0F8FF
240 255 240 #F0FFF0
240 255 255 #F0FFFF
244 164  96 #F4A460
245 222 179 #F5DEB3
245 245 220 #F5F5DC
245 245 245 #F5F5F5
245 255 250 #F5FFFA
248 248 255 #F8F8FF
250 128 114 #FA8072
250 235 215 #FAEBD7
250 240 230 #FAF0E6
250 250 210 #FAFAD2
253 245 230 #FDF5E6
255   0   0 #FF0000
255   0 255 #FF00FF
255  20 147 #FF1493
255  69   0 #FF4500
255  99  71 #FF6347
255 105 180 #FF69B4
255 127  80 #FF7F50
255 140   0 #FF8C00
255 160 122 #FFA07A
255 165   0 #FFA500
255 182 193 #FFB6C1
255 192 203 #FFC0CB
255 215   0 #FFD700
255 218 185 #FFDAB9
255 222 173 #FFDEAD
255 228 181 #FFE4B5
255 228 196 #FFE4C4
255 228 225 #FFE4E1
255 235 205 #FFEBCD
255 239 213 #FFEFD5
255 240 245 #FFF0F5
255 245 238 #FFF5EE
255 248 220 #FFF8DC
255 250 205 #FFFACD
255 250 240 #FFFAF0
255 250 250 #FFFAFA
255 255   0 #FFFF00
255 255 224 #FFFFE0
255 255 240 #FFFFF0
255 255 255 #FFFFFF
        """


def rect_path(self, x, y, width, height, fill_color, stroke_color='black'):
    x0 = x
    y0 = y
    x1 = x0 + width
    y1 = y0 + height

    # Build path box string
    d = f'M {x0} {y0} {x0} {y1} {x1} {y1} {x1} {y0} z'
    # inkex.errormsg(d)

    my_path = PathElement()
    my_path.set('d', d)
    my_path.style['stroke'] = stroke_color
    my_path.style['stroke-width'] = '0.5'
    my_path.style['fill'] = fill_color

    return my_path


def create_palette_squares(self, color_dict, layer_name):
    parent = self.svg
    palette_layer = Layer()

    palette_name = getattr(self.options, 'palette_name', 'Web_Safe')

    palette_layer_id = 'palette_squares_' + str(time.time()).replace('.', '')
    label = palette_name + '_' + str(time.time()).replace('.', '')
    palette_layer.set('inkscape:label', label)
    palette_layer.set('id', palette_layer_id)

    entry_x = 0
    shift_y = ((math.floor(len(color_dict.values()) / 40)) * 5) + 5
    entry_count = 0
    shift_x = 0
    total_dict_len = len(color_dict.keys())
    # y_shift_factor = int(total_dict_len / 40)

    for entry in color_dict.values():
        entry_y = (math.floor(entry_count / 40) * 5) - shift_y
        entry_x = (entry_count * 5) - shift_x
        # inkex.errormsg(entry_y)
        entry_color = rgb_tuple_to_hex(self, entry['rgba'])
        entry_rect = rect_path(self, entry_x, entry_y, 5, 5, entry_color)
        palette_layer.append(entry_rect)
        entry_x += 5
        entry_x = entry_x - shift_x
        entry_count += 1
        shift_x = int(entry_count / 40) * 200

    parent.append(palette_layer)

def recolor_object(self, drawable_children, elements_dict, press_attr):
    for child in drawable_children:
        child_id = child.get_id()

        if press_attr not in elements_dict[child_id]:
            continue

        if 'url' in elements_dict[child_id][press_attr]:
            continue
        if 'none' in elements_dict[child_id][press_attr]:
            continue

        child.style.set_color(elements_dict[child_id][f'cc_{press_attr}'], press_attr)

def populate_closest_color_dict(self, closest_color_dict, fixed_palette_dict, elements_dict, press_attr):
    for item in elements_dict.keys():

        if press_attr not in elements_dict[item]:
            continue

        item_press_attr = elements_dict[item][press_attr]

        if 'url' in item_press_attr:
            continue
        if 'none' in item_press_attr:
            continue

        pil_rgb_color = ImageColor.getrgb(item_press_attr)

        if pil_rgb_color in closest_color_dict.keys():
            elements_dict[item][f'cc_{press_attr}'] = closest_color_dict[pil_rgb_color]
            continue

        closest_color = compare_color_to_color_list(self, pil_rgb_color, fixed_palette_dict)

        try:
            closest_color_rgb = closest_color[1][:3]
        except:
            inkex.errormsg('Invalid Custom Colour Weight Combination')
            sys.exit()
        closest_color_dict[pil_rgb_color] = closest_color_rgb
        elements_dict[item][f'cc_{press_attr}'] = closest_color_rgb

    return closest_color_dict

def gimp_palette_text_to_dict(self, gimp_palette_text):
    gimp_palette_dict = {}
    gimp_palette_text = gimp_palette_text.replace('\t', ' ')
    color_data = gimp_palette_text.split('\n#')[-1]
    # inkex.errormsg(f'Colour Data: {color_data}')
    text_lines = color_data.splitlines()
    # remove the first line which will be either blank or a comment
    text_lines.pop(0)

    text_lines = [i.strip() for i in text_lines]

    # inkex.errormsg(f'text lines: {text_lines}')

    palette_list = list(filter(None, text_lines))

    palette_list = [" ".join(i.split()) for i in palette_list]

    color_index = 0

    for color_entry in palette_list:
        color_label = ' '.join(color_entry.split()[3:])

        # inkex.errormsg(f'Gimp Colour Label: {color_label}')

        color_rgba_list = color_entry.split()[:3]
        color_rgba_list.append(255)
        # inkex.errormsg(f'Gimp color_rgba_list: {color_rgba_list}')
        color_rgba_list_int = [int(i) for i in color_rgba_list]
        color_rgba_tuple = tuple(color_rgba_list_int)
        # inkex.errormsg(f'Gimp RGBA Tuple: {color_rgba_tuple}')
        gimp_palette_dict[color_index] = {}
        gimp_palette_dict[color_index]['label'] = color_label
        gimp_palette_dict[color_index]['rgba'] = color_rgba_tuple

        color_index += 1

    return gimp_palette_dict


def rgb_tuple_to_hex(self, rgb_tuple):
    hex_list = [hex(i).strip().lstrip('0x').zfill(2).upper() for i in rgb_tuple[:3]]
    hex_color = '#' + ''.join(hex_list)
    return hex_color


def open_gimp_palette_file(self, filepath):
    try:
        with open(filepath, 'r') as gimp_palette_file:
            gimp_palette_text = gimp_palette_file.read()
    except:
        inkex.errormsg('Unable to open gimp palette file')
        sys.exit()

    if 'GIMP Palette' not in gimp_palette_text:
        inkex.errormsg('Invalid GIMP Palette file')
        sys.exit()

    palette_filename = os.path.basename(gimp_palette_file.name)

    self.options.palette_name = palette_filename
    return gimp_palette_text_to_dict(self, gimp_palette_text)


def get_euclidean_distance(rgb1, rgb2, r_weight=1, g_weight=1, b_weight=1):
    rgb1 = [int(i) for i in rgb1]
    rgb2 = [int(i) for i in rgb2]

    distance = math.sqrt(((rgb2[0] - rgb1[0])*r_weight) ** 2 +
                         ((rgb2[1] - rgb1[1])*g_weight) ** 2 +
                         ((rgb2[2] - rgb1[2])*b_weight) ** 2)

    return distance


def compare_color_to_color_list(self, rgb1, color_list):
    closest_color_match = [rgb1, None]
    current_distance = 999

    r_weight = self.r_weight
    g_weight = self.g_weight
    b_weight = self.b_weight

    for list_color in color_list.values():

        distance = get_euclidean_distance(rgb1, list_color['rgba'], r_weight, g_weight, b_weight)
        if distance < current_distance:
            current_distance = distance
            closest_color_match[1] = list_color['rgba']

    return closest_color_match


# Build a palette from selected gradient objects
def palette_from_gradient_objects(self, selection_list):

    stop_list = []

    palette_dict = {}

    for selected_item in selection_list:
        selected_item_style = selected_item.style

        fill = selected_item_style.get('fill')
        if fill is None or 'none' in fill.lower():
            fill = selected_item.get('fill')
            if fill is None or 'none' in fill.lower():
                continue
        if 'url' in fill:
            href = fill.split('#')[1].replace(')', '')
            fill_element = self.svg.getElementById(href)

            xlink = fill_element.get('xlink:href')

            if xlink is None:
                stops = fill_element.xpath('.//svg:stop')
            else:
                fill_element_id = xlink.split('#')[1].replace(')', '')
                fill_element = self.svg.getElementById(fill_element_id)
                stops = fill_element.xpath('.//svg:stop')
            
            stops = [color_from_gradient_stop(self, stop) for stop in stops]
            stop_list += stops

        palette_list = list(set(stop_list))

        palette_index = 0
        for entry in palette_list:
            pil_rgb_color = ImageColor.getrgb(entry)
            palette_dict[palette_index] = {}
            palette_dict[palette_index]['label'] = entry
            palette_dict[palette_index]['rgba'] = list(pil_rgb_color) + [255]
            palette_index += 1


    return palette_dict

def list_gradients(self, svg):

    gradient_list = svg.xpath('.//svg:linearGradient')

    true_gradient_list = []

    for gradient in gradient_list:
        # lets discount the intermediate positioning gradient tag
        if 'xlink' not in gradient.tostring().decode('utf-8'):
            true_gradient_list.append(gradient)

    return true_gradient_list

def color_from_gradient_stop(self, stop):

        stop_style_color = stop.style.get('stop-color')

        if stop_style_color is not None:
            return stop_style_color
        else:
            stop_attr_color = stop.get('stop-color')
            if stop_attr_color is not None:
                return stop_attr_color
            else:
                return None

def gradients_to_dict(self, gradient_list):

    gradient_color_list = []

    gradient_dict = {}

    for gradient in gradient_list:

        gradient_id = gradient.get_id()

        gradient_dict[gradient_id] = {}

        gradient_stop_list = gradient.xpath('.//svg:stop')
        for stop in gradient_stop_list:
            stop_color = color_from_gradient_stop(self, stop)
            if stop_color is not None:
                gradient_color_list.append(stop_color)


    palette_list = list(set(gradient_color_list))

    palette_dict = {}
    palette_index = 0
    for entry in palette_list:
        pil_rgb_color = ImageColor.getrgb(entry)
        palette_dict[palette_index] = {}
        palette_dict[palette_index]['label'] = entry
        palette_dict[palette_index]['rgba'] = list(pil_rgb_color) + [255]
        palette_index += 1

    return palette_dict

def pil_pixels_to_dict(self, image):

    color_index = 0

    im = Inklin.image_to_pil(self, image)
    # inkex.errormsg(im)
    im_p = im.convert('P')
    im_rgb = im_p.convert('RGB')
    colors = im_rgb.getcolors()

    # inkex.errormsg(colors)

    color_list = [list(color[1]) for color in colors]
    [color.append(255) for color in color_list]

    fixed_palette_dict = {}
    for color_entry in color_list:
        color_rgba_tuple = tuple(color_entry)
        fixed_palette_dict[color_index] = {}
        fixed_palette_dict[color_index]['label'] = color_index
        fixed_palette_dict[color_index]['rgba'] = color_rgba_tuple
        color_index += 1

    return fixed_palette_dict

def make_style_dict(self, element_list):

    element_style_dict = {}


    for element in element_list:
        element_id = element.get_id()
        element_style_dict[element_id] = {}

        # element_composed_style = element.composed_style()
        element_composed_style = element.specified_style()

        for style_key in element_composed_style.keys():
            element_style_dict[element_id][style_key] = element_composed_style[style_key]

    return element_style_dict


class IndexedColour(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--main_notebook", type=str, dest="main_notebook", default=0)

        pars.add_argument("--palette_source_radio", type=str, dest="palette_source_radio", default='web_safe')

        pars.add_argument("--gimp_palette_filepath", type=str, dest="gimp_palette_filepath", default=None)

        pars.add_argument("--palette_squares_cb", type=str, dest="palette_squares_cb")

        pars.add_argument("--apply_palette_cb", type=str, dest="apply_palette_cb")

        pars.add_argument("--apply_to_radio", type=str, dest="apply_to_radio", default='visual')

        pars.add_argument("--euclidean_weighting_radio", type=str, dest="euclidean_weighting_radio", default='1_1_1')

        pars.add_argument("--r_weight_int", type=int, dest="r_weight_int", default=1)
        pars.add_argument("--g_weight_int", type=int, dest="g_weight_int", default=1)
        pars.add_argument("--b_weight_int", type=int, dest="b_weight_int", default=1)

    def effect(self):

        if self.options.euclidean_weighting_radio == '1_1_1':
            self.r_weight = self.g_weight = self.b_weight = 1
        elif self.options.euclidean_weighting_radio == '3_4_2':
            self.r_weight = 3
            self.g_weight = 4
            self.b_weight = 2
        elif self.options.euclidean_weighting_radio == '2_4_3':
            self.r_weight = 2
            self.g_weight = 4
            self.b_weight = 3
        elif self.options.euclidean_weighting_radio == 'custom':
            self.r_weight = self.options.r_weight_int
            self.g_weight = self.options.g_weight_int
            self.b_weight = self.options.b_weight_int

        # inkex.errormsg(f'{self.r_weight}  {self.g_weight}  {self.b_weight}')

        selected = self.svg.selected

        if self.options.apply_palette_cb == 'false' and self.options.palette_squares_cb == 'false':
            inkex.errormsg('Nothing To Do :(')
            return

        if self.options.palette_source_radio == 'web_safe':

            self.options.palette_name = 'web_safe_palette'

            fixed_palette_dict = gimp_palette_text_to_dict(self, web_safe_gimp_palette)
        elif self.options.palette_source_radio == 'file':
            fixed_palette_dict = open_gimp_palette_file(self, self.options.gimp_palette_filepath)

        elif self.options.palette_source_radio == 'doc_gradients':

            self.options.palette_name = 'gradient_palette'

            gradient_list = list_gradients(self, self.svg)
            fixed_palette_dict = gradients_to_dict(self, gradient_list)
            if fixed_palette_dict == {}:
                inkex.errormsg('No Gradients Found in Document')
                return

        elif self.options.palette_source_radio == 'selected_gradients':

            self.options.palette_name = 'gradient_palette'

            if len(selected) < 1:
                inkex.errormsg('Nothing Selected')
                return

            fixed_palette_dict = palette_from_gradient_objects(self, selected)
            if fixed_palette_dict == {}:
                inkex.errormsg('No Gradients Found in Selection')
                return

        elif self.options.palette_source_radio == 'pil_image_palette':

            self.options.palette_name = 'pil_palette'

            if len(selected) < 1:
                inkex.errormsg('Nothing Selected')
                return

            if selected[0].TAG == 'image':

                fixed_palette_dict = pil_pixels_to_dict(self, selected[0])

                if fixed_palette_dict == {}:
                    inkex.errormsg('No Colours Found in Selection')
                    return
            else:
                inkex.errormsg('First selected item is not an image')
                return


        selected = self.svg.selected

        if self.options.apply_palette_cb == 'true':

            drawable_children = self.svg.xpath(
                '//svg:circle | //svg:ellipse | //svg:line | //svg:path | //svg:text | //svg:polygon | //svg:polyline '
                '| //svg:rect | //svg:use')

            elements_dict = make_style_dict(self, drawable_children)

            closest_color_dict = {}

            if self.options.apply_to_radio == 'fill' or self.options.apply_to_radio == 'fill_stroke':

                # closest_color_dict is returned so that we do not repeat some comparisons in the stroke section
                closest_color_dict = populate_closest_color_dict(self, closest_color_dict, fixed_palette_dict, elements_dict, 'fill')

                recolor_object(self, drawable_children, elements_dict, 'fill')


            if self.options.apply_to_radio == 'stroke' or self.options.apply_to_radio == 'fill_stroke':

                closest_color_dict = populate_closest_color_dict(self, closest_color_dict, fixed_palette_dict, elements_dict, 'stroke')

                recolor_object(self, drawable_children, elements_dict, 'stroke')


        if self.options.palette_squares_cb == 'true':
            # Create a sampler layer
            create_palette_squares(self, fixed_palette_dict, 'test_layer')

if __name__ == '__main__':
    IndexedColour().run()
